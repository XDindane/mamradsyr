<?php

/**
 * Description of DummyTasks
 *
 * @author Denis Mahmúd <denis.mahmud@gmail.com>
 * @since 2014-10-14 
 */

namespace CronnerTasks;

use Nette\Object;
use App\Models;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;


class DummyTasks extends Object {

	/** @var Models\Zastavy  */
	public $zastavyModel;
	
	/**
	 * @var Context\Parametrs\Mails\To
	 */
	public $mails;



	public function __construct(Models\Zastavy $zastavy, $mail) {
		$this->mails = $mail;
		$this->zastavyModel = $zastavy;

	}



	/**
	 * @cronner-task E-mail sending
	 * @cronner-period 20 hours
	 * @cronner-days working days, weekend
	 * @cronner-time 00:00 - 23:59
	 */
	public function sendEmails() {
		$data = $this->zastavyModel->getExpiredData();
		if ($data->fetch()) {
			try {
				$mail = new Message;
				$mail->setFrom('Mamradsyr <noreply@mamradsyr.cz>');
				foreach($this->mails['to'] as $receipt) {
					$mail->addTo($receipt);
				}
				$mail->setSubject('Zástavy - blížící se a nesplacené zástavy');

				$template = '<h2>Nesplacené a blížící se zástavy</h2>';
				foreach ($data as $key => $values) {
					$template .= '<p>';

					if (!empty($values->name)) {
						$template .= 'Jméno: ' . $values->name . '<br>';
					}
					if (!empty($values->telefon)) {
						$template .= 'Telefon: ' . $values->telefon . '<br>';
					}
					if (!empty($values->znacka)) {
						$template .= 'Automobil: ' . $values->znacka . '<br>';
					}
					if (!empty($values->model)) {
						$template .= 'Model: ' . $values->model . '<br>';
					}
					if (!empty($values->rezervacni_poplatek)) {
						$template .= 'Poplatek: ' . $values->rezervacni_poplatek . ' Kč <br>';
					}
					if (!empty($values->rezervacni_poplatek_splatnost)) {
						if(time() > strtotime($values->rezervacni_poplatek_splatnost))  {
							$template .= '<span style="color:red;font-weight:bold"> ';
						} else { $template .= '<span style="color:#ffc851;font-weight:bold"> '; }
						$template .= 'Splatnost: ' . $values->rezervacni_poplatek_splatnost->format('j. m. Y') . '</span><br>';
					}
					if (!empty($values->pobocka)) {
						$template .= 'Pobočka: ' . (($values->pobocka == 1) ? 'Praha' : 'Pardubice') . '<br>';
					}

					$template .= '</p>';
				}


				$mail->setHtmlBody($template);

				$mailer = new SendmailMailer;
				$mailer->send($mail);
			} catch (Exception $exc) {
				echo $exc->getTraceAsString();
			}
		}

	}



}
