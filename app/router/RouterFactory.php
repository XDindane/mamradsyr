<?php

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route,
	Nette\Application\Routers\SimpleRouter;
use App\Models;

/**
 * Router factory.
 */
class RouterFactory
{
	
	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();
		$router[] = new Route('[<presenter>/]<action>[/<id>]', 'Event:default');
		return $router;
	}

}
