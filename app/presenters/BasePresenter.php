<?php

namespace App\Presenters;

use Nette;
use App\Models;



/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

	/** @var string */
	protected $dbVersion;



	public function startup() {
		parent::startup();
		
		$this->dbVersion = $this->context->parameters['dbPrefix'];
		$this->template->dbVersion = $this->dbVersion;

	}
	
	



}
