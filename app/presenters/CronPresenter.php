<?php

/**
 * Description of CronPresenter
 *
 * @author Denis Mahmúd <denis.mahmud@gmail.com>
 * @since 2014-10-14 
 */

namespace App\Presenters;

use stekycz\Cronner\Cronner;
use Nette;
use stekycz\Cronner\Tasks\Task;



class CronPresenter extends BasePresenter {

	/**
	 * @var \stekycz\Cronner\Cronner
	 * @inject
	 */
	public $cronner;



	public function renderDefault() {
		$this->cronner->onTaskFinished[] = function (Cronner $cronner, Task $task) {

			//$this->flashMessage('Task ' . $task->getName() . ' has been finished.','success');
		};
		$this->cronner->onTaskError[] = function (Cronner $cronner, \Exception $exception, Task $task) {
			//$this->flashMessage('Task "' . $task->getName() . '" has been stoped by an error: ' . $exception->getMessage(),'error');
			dump($exception->getMessage());
		};
		$this->cronner->run();
		$this->terminate();
	}



}
