<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Sign in/out presenters.
 */

use App\Models;
class SignPresenter extends BasePresenter
{

	
	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = new Nette\Application\UI\Form;
		$form->addText('username', '')
			->setRequired('Please enter your username.');

		$form->addPassword('password', '')
			->setRequired('Please enter your password.');

		$form->addCheckbox('remember', 'zůstat přihlašen');

		$form->addSubmit('send', 'Přihlásit se');

		// call method signInFormSucceeded() on success
		$form->onSuccess[] = $this->signInFormSucceeded;
		return $form;
	}


	public function signInFormSucceeded($form, $values)
	{
		if ($values->remember) {
			$this->getUser()->setExpiration('14 days', FALSE);
		} else {
			$this->getUser()->setExpiration('1 day', TRUE);
		}

		try {
			$this->getUser()->login($values->username, $values->password);
			$this->redirect('Event:');

		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
		}
	}

}
