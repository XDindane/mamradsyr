<?php



/**
 * Description of RozpisPresenter
 *
 * @author Denis Mahmúd <denis.mahmud@gmail.com>
 * @since 2015-01-15 
 */

namespace App\Presenters;

use EventCalendar\Simple\SimpleCalendar;
use EventCalendar\Simple\EventCalendar;

class RozpisPresenter extends SecuredPresenter{
	//put your code here
	public function startup() {
		parent::startup();

	}
	
	protected function beforeRender() {
		parent::beforeRender();

	}

	
	public function createComponentCalendar() {
    $cal = new SimpleCalendar();
	$cal->setLanguage(SimpleCalendar::LANG_CZ);
	$cal->setFirstDay(SimpleCalendar::FIRST_MONDAY);
	$cal->setOptions(array(EventCalendar::OPT_SHOW_BOTTOM_NAV => false));
	
//	$lazy = $this->context->eventCalendarRepository;
//	$lazy->initItems($this->user->id);
//	$cal->setEvents($events);
    return $cal;
}

}
