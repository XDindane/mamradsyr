<?php



/**
 * Description of KontaktyPresenter
 *
 * @author Denis Mahmúd <denis.mahmud@gmail.com>
 * @since 2014-09-19 
 */
namespace App\Presenters;

use App\Models;
use App\Forms;

class KontaktyPresenter extends SecuredPresenter{

    /** @var  Models\Kontakty @inject */
    public $contactModel;
    
    public $kategorie = 1 ;
    public function startup() {
        parent::startup();
    }
    

    public function renderDefault() {
        $this->template->contacts = $this->getContacts();
        $this->template->allContacts = $this->getAllContacts();
        $this->template->kategorie = $this->kategorie;
     
    }
	
	public function actionDefault() {
	}

    /**
     * Componenta ContactForm
     * @param type $name
     * @return \App\Forms\zastavyForm
     */
    public function createComponentContactForm($name) {
        $form = new Forms\ContactForm($this->contactModel, $this->db);
        return $form;
    }
    
    /**
     * returns contacts by kategory and pobcoka
     * @return App/Models/Kontakty
     */
    public function getContacts() {
        return $this->contactModel->getContacts($this->pobocka, $this->kategorie);
    }
    
    /**
     * returns all contacts by pobocka
     * @return App/Models/Kontakty
     */
    public function getAllContacts() {
        return $this->contactModel->getByPobocka($this->pobocka)->group('kategorie')->order('kategorie ASC, name ASC');
    }
     
        public function handleDelete($id) {
        
        $this->contactModel->delete($id);
        
            if($this->isAjax()) {
                $this->redrawControl('link');
                $this->redrawControl('contacts');
            } else {
                $this->redirect('this');
            }
    }
    
        public function handleEdit($id) {
            $defaults = $this->loadItem($id);
            
           $this['contactForm']['form']->getElementPrototype()->class = 'show';
           $this['contactForm']['form']->setDefaults($defaults); 
        if($this->isAjax()) {
           $this->redrawControl('contactFormSnippet');
        } else {
            $this->redirect('this');
        }
    }
    
    public function handleChangeCategory($id) {
                
        $this->kategorie = $id;
        
        if($this->isAjax()) {
            $this->redrawControl('link');
                $this->redrawControl('contacts');
            } else {
                $this->redirect('this');
            }  
    }
    
    
     protected function loadItem($id) {
        $defaults = $this->contactModel->getById($id)->fetch();

        if (!$defaults) {
            $this->flashMessage("Item with id $id does not exist", 'error');
            $this->redirect('this'); // aka items list
        }
        return $defaults;
    }
    
    
    
}
