<?php

namespace App\Presenters;

use Nette,
	App\Models;
use Nette\Application\UI\Multiplier;



/**
 * Homepage presenter.
 */
class SecuredPresenter extends BasePresenter {

	/** @var  Models\User @inject */
	public $userModel;

	/** @var  \\Nette\Database\Context @inject */
	public $db;
	
	public $pobocka;

	/** @var \DK\Menu\UI\IControlFactory @inject */
	public $menuFactory;



	public function startup() {
		parent::startup();
		/**
		 * @description kontrola jestli je uzivatel prihlasen
		 * odhlaseni za neaktivitu 
		 */
		if (!$this->user->isLoggedIn()) {
			if ($this->user->getLogoutReason() === \Nette\Security\User::INACTIVITY) {
				$this->flashMessage('Byl jste odhlášen z důvodu své neaktivity. Prosím, přihlašte se znovu.');
				$this->redirect('Sign:in');
			} else {
				$this->redirect('Sign:in');
			}
		}
		
		
		$this->pobocka = $this->user->getIdentity()->data['pobocka'];
		
	}



	/**
	 * 
	 */
	protected function beforeRender() {
		parent::beforeRender();
	}

	public function render() {
		
	}

	/**
	 * Sign out user
	 */
	public function actionOut() {
		$this->getUser()->logout(TRUE);
		$this->redirect('in');

	}



	/**
	 * 
	 * @return \DK\Menu\UI\IControlFactory
	 */
	public function createComponentMenu() {
		return $this->menuFactory->create();

	}



	/**
	 * Pouziva se na vice mistech, vraci classu podle datumu
	 * @param date $time
	 * @return string
	 */
	public function evaluateTime($time) {

		if ((time() < strtotime($time)) && (time() - strtotime($time) >= -172800)) {
			$class = 'warning';
		} elseif (time() > strtotime($time)) {
			$class = 'red';
		} elseif (time() - strtotime($time) <= -172800) {
			$class = '';
		}

		return $class;

	}



}
