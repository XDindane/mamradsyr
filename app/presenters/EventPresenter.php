<?php

/**
 * Description of PlanovacPresenter
 *
 * @author denis
 * @since 2014-09-17
 */

namespace App\Presenters;

use App\Models;
use App\Forms;



class EventPresenter extends SecuredPresenter {

	/** @var  Models\Events @inject */
	public $eventModel;

	/** @var string $status  */
	protected $splneno = 0;
	protected $userId = NULL;



	public function startup() {
		parent::startup();

	}



	protected function beforeRender() {
		parent::beforeRender();
		if ($this->userId !== NULL) {
			$this['selectUser']['user']->setDefaultValue($this->userId);
		}

	}



	public function renderDefault() {
		$this->template->eventsWeekly = $this->getEvent(1);
		$this->template->eventsDaily = $this->getEvent(0);
		$this->template->active = $this->splneno;

	}



	public function actionDefault() {

	}



	/**
	 * 
	 * @param boolean $splneno
	 * @param smallint $pobocka
	 */
	public function getEvent($type) {
		$sel = $this->eventModel->getEventByCondition(
				$this->splneno, $this->pobocka, $this->userId, $type
			)->order('date_end ASC');

		return $sel;

	}



	/**
	 * 
	 * @param type $name
	 * @return \App\Forms\eventsForm
	 */
	public function createComponentAddEventForm($name) {
		$form = new Forms\eventsForm($this->userModel, $this->eventModel, $this->db);
		return $form;

	}



	protected function createComponentSelectUser($name) {
		$form = new \Nette\Application\UI\Form;
		$form->addSelect('user', NULL, $this->userModel->getToForm($this->pobocka))
				->setPrompt('filtrovat uživatele')
				->getControlPrototype()->id = 'selectUser';
		$form->getElementPrototype()->class = 'right-style';
		return $form;

	}



	public function handleDelete($id) {

		$this->eventModel->delete($id);
		if ($this->isAjax()) {
			$this->redrawControl('event');
		} else {
			$this->redirect('this');
		}

	}



	public function handleEdit($id) {

		$defaults = $this->loadItem($id);
		$this['addEventForm']['form']->getElementPrototype()->class = 'show';
		$this['addEventForm']['form']->setDefaults($defaults);
		$this['addEventForm']['form']['dodatek']->setDisabled(FALSE);
		$this['addEventForm']['form']['date_start']->setDefaultValue($defaults->date_start->format('j. n. Y'));
		$this['addEventForm']['form']['date_end']->setDefaultValue($defaults->date_end->format('j. n. Y'));

		if ($this->isAjax()) {
			$this->redrawControl('link');
		}

	}



	protected function loadItem($id) {
		$defaults = $this->eventModel->getByCondition(array('id' => $id))->fetch();

		if (!$defaults) {
			$this->flashMessage("Item with id $id does not exist", 'error');
			$this->redirect('this'); // aka items list
		}
		return $defaults;

	}



	/**
	 * 
	 * @param type $id
	 */
	public function handleSuccess($idTask) {
		$this->eventModel->update($idTask,
			array('zpracoval' => $this->user->getId(), 'splneno' => 1));
		
		if ($this->isAjax()) {
			$this->redrawControl('event');
		} else {
			$this->redirect('this');
		}

	}



	public function handleHotove($splneno) {

		$this->splneno = $splneno;

		if ($this->isAjax()) {
			$this->redrawControl('link');
			$this->redrawControl('event');
		} else {
			$this->redirect('this');
		}

	}



	public function handleChangeUserView($iduser) {
		if (empty($iduser)) {
			$this->userId = NULL;
		} else {
			$this->userId = $iduser;
			$this['selectUser']['user']->setDefaultValue($iduser);
		};

		if ($this->isAjax()) {
			$this->redrawControl('link');
			$this->redrawControl('event');
		} else {
			$this->redirect('this');
		}

	}



}
