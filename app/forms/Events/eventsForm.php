<?php

/**
 * Description of eventsForm
 *
 * @author denis
 * @since 2014-09-17
 */

namespace App\Forms;

use Nette\Forms\Form;
use Nette\Application\UI;
use App\Models;
use RadekDostal\NetteComponents\DateTimePicker\DatePicker;
use Nette\Database\Context;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;



class eventsForm extends UI\Control {

	/** @var Models/User */
	private $userModel;

	/** @var Models/Event */
	private $eventModel;

	/** $var Nette/database/context */
	private $db;



	public function __construct(Models\User $userModel, Models\Events $eventModel, \Nette\Database\Context $db) {
		parent::__construct();
		$this->userModel = $userModel;
		$this->eventModel = $eventModel;
		$this->db = $db;
		$this->loadContainer();

	}



	public function injectDatabase(Context $database) {
		$this->db = $database;

	}



	private function loadContainer() {
		Form::extensionMethod('addDatePicker',
			function(Form $_this, $name, $label, $cols = NULL, $maxLength = NULL) {
			return $_this[$name] = new DatePicker($label, $cols, $maxLength);
		});

	}



	public function render() {
		$this->template->setFile(__DIR__ . '/../form.latte');
		$this->template->render();

	}



	protected function createComponentForm() {
		$form = new UI\Form;

		$form->addText('title', 'Název')
			->setRequired('Vyplnte nazev kontaktu');
		$form->addSelect('typ', 'Typ', array(0 => 'Denní', 1 => 'Týdenní'))
			->setPrompt('Typ Akce')
			->setRequired('Zadejte typ Akce');
		$form->addSelect('priority', 'Priorita',
				array(1 => 'Low', 2 => 'Normal', 3 => 'High'))
			->setPrompt('Zadejte prioritu')
			->setRequired('Vyplnte Prioritu');
		$form->addDatePicker('date_start', 'Datum zahájení', 10, 10);
		$form->addDatePicker('date_end', 'Datum ukončení', 10, 10);
		$form->addTextArea('notes', 'Poznámka');
		$form->addTextArea('dodatek', 'Dodatek')
				->setDisabled(TRUE)
				->getControlPrototype()->id = 'dodatekCategory';
		$form->addSelect('response_user', 'Zodpovědná osoba',
				$this->userModel->getToForm($this->presenter->pobocka))
			->setRequired('Vyplnte zodpovědnou osobu');
		$form->addSubmit('submit', 'Vytvořit událost');
		$form->addHidden('id');

		$form->onSuccess[] = $this->formProccess;
		$form->getElementPrototype()->class = 'hide';
		return $form;

	}



	public function formProccess($form, $values) {
		$values->userId = $this->presenter->user->getId();
		$values->pobocka = $this->presenter->pobocka;
		$values->creator = $this->presenter->user->getId();

		$post = $this->presenter->request->getPost('id');
		if (isset($post['dodatek']))
			$values->dodatek = $post['dodatek'];
		try {
			$this->db->beginTransaction();
			$repository = $this->eventModel;

			if (!empty($values->id)) {
				$repository->update($values->id, $values);
				$newEvent = false;
			} else {
				$repository->insert($values);
				$values->id = $this->db->getInsertId();
				$newEvent = true;
			}

			$this->db->commit();
		} catch (Exception $e) {
			$this->db->rollBack();
			$this->presenter->flashMessage('Nastala chyba při ukládání.', 'error');
			return;
		}
		$this->sendEmail($values, $newEvent);

	}



	protected function sendEmail($values, $newEvent) {

		try {
			$mail = new Message;
			if ($newEvent === true) {
				$subject = 'Plánovač - Byl Vám přidělen nový úkol';
			} else {
				$subject = 'Plánovač - Byla provedena editace přiděleného úkolu';
			}
			$recipient = $this->userModel->getById($values->response_user)->fetch();

			$mail->setFrom('Mamradsyr <' . $this->presenter->context->parameters['mail']['from'] . '>');
			$mail->addTo($recipient->email);
			$mail->setSubject($subject);

			$template = $this->createTemplate();
			$template->setFile(__DIR__ . '/mailTemplate.latte');

			$values->creator = $this->userModel->getById($values->creator)->select('name')->fetch();
			$template->values = $values;
			$mail->setHtmlBody($template);

			$mailer = new SendmailMailer;
			$mailer->send($mail);
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
		}
		$this->presenter->redirect('this');

	}



}
