<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactForm
 *
 * @author Denis Mahmúd <denis.mahmud@gmail.com>
 */
namespace App\Forms;

use Nette\Forms\Form;
use Nette\Application\UI;
use App\Models;
use RadekDostal\NetteComponents\DateTimePicker\DatePicker;
use Nette\Database\Context;


class ContactForm extends UI\Control{
    /** @var Models/User */
    private $userModel;

    /** @var Models/Kontakty */
    private $contactModel;

    /** $var Nette/database/context */
    private $db;

    public function __construct(Models\Kontakty $contactModel, \Nette\Database\Context $db) {
        parent::__construct();

        $this->contactModel = $contactModel;
        $this->db = $db;
    }

    public function injectDatabase(Context $database) {
        $this->db = $database;
    }


    public function render() {
        $this->template->setFile(__DIR__ . '/../form.latte');
        $this->template->render();
    }

    protected function createComponentForm() {
        $form = new UI\Form;
        $form->addSelect('kategorie', 'Kategorie', $this->getCategory() )
                ->setPrompt('zvolte kategorii')
                ->setRequired('Je nutné vybrat kategorii')
                ->getControlPrototype()->id = 'selectCategory';
        
        $form->addText('nazevKategorie', 'nová kategorie' )
                ->setAttribute('readonly', 'readonly')
                ->getControlPrototype()->id = 'newCategory';
        
        $form->addText('name', 'Název');  
        $form->addText('kontaktni_osoba', 'Kontaktní osoba');
        $form->addText('telefon', 'Telefon')
                ->addRule($form::INTEGER, 'telefon musí být číslo');
        $form->addText('email', 'e-mail')
                ->addRule($form::EMAIL, 'zadejte spravný tvar emailu');
        $form->addHidden('id');
        $form->getElementPrototype()->class = 'hide';
        $form->addSubmit('submit', 'uložit kontakt');

        $form->onSuccess[] = $this->formProccess;
        return $form;
    }
    
    private function getCategory() {
        $output = array(  );
        $output[0] = 'Nová Kategorie';
        $db = $this->db->table($this->presenter->context->parameters['dbPrefix']. 'kategorie')->where('pobocka', $this->presenter->pobocka)->fetchPairs('id', 'nazev');
        foreach($db as $key => $value) {
            $output[$key] = $value;
        }
        return $output;
                
    }
    
    /**
     * 
     * @param Nette/Form $form
     * @param array $values
     * 
     */
    public function formProccess($form, $values) {
        if (!$form['submit']->isSubmittedBy())
            return;
        
         $values->pobocka = $this->presenter->pobocka;
        try {
            $this->db->beginTransaction();
            
          if($values->kategorie == 0) {
             $insert = $this->db->table($this->presenter->context->parameters['dbPrefix'].'kategorie')->insert(
                     array(
                       'nazev' => $values->nazevKategorie, 
                        'pobocka' => $values->pobocka
                     ));
             $values->kategorie = $insert->id;
             
         }
         unset($values->nazevKategorie);
            
            $repository = $this->contactModel;

            if (!empty($values->id)) {
                $repository->update($values->id, $values);
            } else {
                $repository->insert($values);
                $values->id = $this->db->getInsertId();
            }

            $this->db->commit();
        } catch (Exception $e) {
            $this->db->rollBack();
            $this->presenter->flashMessage('Nastala chyba při ukládání.', 'error');
            return;
        }
        $this->presenter->redirect('default');
    }
}
