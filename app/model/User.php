<?php
/**
 * Description of User Model
 *
 * @author denis
 * @since 2014-09-02
 */
namespace App\Models;
class User extends Base{
    
    /**
     * @return user by name
     * @param string
     */
    public function getByName($name)
    {
        return $this->db->user("name", $name)->fetch();
    }
    
    /**
     * return pairs to form
     * @return array list($id, $name)
     */
    public function getToForm($pobocka = NULL) {
        $sel = $this->getTable();
        if($pobocka !== NULL) {
            $sel->where('pobocka', $pobocka);
        }
        
        return $sel->fetchpairs('id', 'name');
    }
}
