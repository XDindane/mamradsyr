<?php

/**
 * Description of BaseModel
 *
 * @author denis
 * @since 2014-09-02
 */

namespace App\Models;

use Nette;
use Nette\Database\Context;
use Nette\Object;



abstract class Base extends Object {

	/** @var Nette\Database\Connection */
	protected $db;

	/** @var string */
	private $tableName;
	
	/** @var array */
	protected $dbVersion;



	/**
	 * @param Context
	 */
	public function __construct(Context $database, \Nette\DI\Container $systemContainer) {
		$this->db = $database;
		$this->dbVersion = $systemContainer->getParameters();
		$this->tableName = $this->tableNameByClass(get_class($this), $this->dbVersion['dbPrefix']);

	}



	/**
	 * Určí tabulku dle názvu třídy
	 * @param string
	 * @return string
	 * @result: Pages => pages, ArticleTag => article_tag
	 */
	private function tableNameByClass($className, $prefix) {
		$tableName = explode("\\", $className);
		$tableName = lcfirst(array_pop($tableName));

		$replace = array(); // A => _a
		foreach (range("A", "Z") as $letter) {
			$replace[$letter] = "_" . strtolower($letter);
		}
		return $prefix . strtr($tableName, $replace);

	}



	/**
	 * 
	 * @return Object returns table
	 */
	public function getTable() {
		return $this->db->table($this->tableName);

	}



	/**
	 * @return query by condition
	 * @param array $cond
	 * @return Nette\Database\Connection
	 */
	public function getByCondition(array $cond) {
		return $this->getTable()->where($cond);

	}



	/**
	 * Updates a collumns in table
	 * @param integer $id
	 * @param array $data
	 */
	public function update($id, $data) {
		$this->getByCondition(array('id' => $id))->update($data);

	}



	/**
	 * Deletes table
	 * @param integer $id
	 */
	public function delete($id) {
		$this->getByCondition(array('id' => $id))->delete();

	}



	/**
	 * Inserts data to table
	 * @param array $data
	 */
	public function insert($data) {
		$this->getTable()->insert($data);

	}


	/**
	 * Gets data by ID
	 * @param integer $id
	 * @return ActiveRow
	 */
	public function getById($id) {
		return $this->getByCondition(array('id' => $id));

	}


	/**
	 * Gets data by Pbocoka
	 * @param integer $id
	 * @return ActiveRow
	 */
	public function getByPobocka($id) {
		return $this->getByCondition(array('pobocka' => $id));

	}



}
