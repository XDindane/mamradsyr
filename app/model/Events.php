<?php

/**
 * Description of Events
 *
 * @author denis
 * @since 2014-09-17
 */

namespace App\Models;



class Events extends Base {

    
	public function getEventByCondition($splneno = FALSE, $pobocka = NULL, $user = NULL, $typ) {
		$data = array(
			'splneno' => $splneno,
			'typ' => $typ
		);

		if ($pobocka !== NULL)
			$data['pobocka'] = $pobocka;
		if ($user !== NULL)
			$data['response_user'] = $user;

		return $this->getByCondition($data);

	}



}
