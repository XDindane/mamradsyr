<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Kontakty
 *
 * @author Denis Mahmúd <denis.mahmud@gmail.com>
 */
namespace App\Models;
class Kontakty extends Base{
    
    public function getContacts($pobocka, $kategorie) {
        return $this->getByPobocka($pobocka)->where('kategorie', $kategorie);       
    }
}
